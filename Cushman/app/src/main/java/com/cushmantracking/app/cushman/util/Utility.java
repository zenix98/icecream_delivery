package com.cushmantracking.app.cushman.util;

import android.content.Context;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.SystemClock;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;

import com.cushmantracking.app.cushman.bean.LatLang;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.List;

/**
 * Created by amitrai on 24/5/17.
 * this class is used for :-
 */

public class Utility {


    private static final String DIRECTION_URL = "https://maps.googleapis.com/maps/api/directions/";
    private static final String ORIGIN = "origin=";
    private static final String DESTINATION = "destination=";
    private static final String SENSOR_FALSE = "sensor=false";
    private static final String WAYPOINTS_VIA = "waypoints=via:";
    private static final String TYPE_JSON = "json";



    /**
     * shows toast to the view.
     */
    public void showSnackBar(CoordinatorLayout layout, String message){
        Snackbar snackbar = Snackbar
                .make(layout, ""+message, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    /**
     * @param context
     * @return true, if Internet available otherwise false
     */
    public static boolean isInternetAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] networkInfos = connectivity.getAllNetworkInfo();
            if (networkInfos != null)
                for (NetworkInfo info : networkInfos) {
                    if (info.getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
        }
        return false;
    }


    public void animateMarker(GoogleMap map, final Marker m, final LatLng toPosition, final boolean hideMarke) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = map.getProjection();
        Point startPoint = proj.toScreenLocation(m.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 5000;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                m.setPosition(new LatLng(lat, lng));

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else
                    m.setVisible(!hideMarke);


            }
        });
    }



    public void rotateMarker(final Marker marker, final float toRotation, final float st) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final float startRotation = st;
        final long duration = 1555;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed / duration);

                float rot = t * toRotation + (1 - t) * startRotation;

                marker.setRotation(-rot > 180 ? rot / 2 : rot);
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
            }
        });
    }


    /**
     *
     * @param origin start location
     * @param dest   end location
     * @param markerPoints total list of marker points.
     * @return direction url including waypoints.
     */


    public String getDirectionsUrl(LatLng origin,LatLng dest,List<LatLang> markerPoints){


        // Origin of route
        String str_origin = ORIGIN+origin.latitude+","+origin.longitude;

        // Destination of route
        String str_dest = DESTINATION+dest.latitude+","+dest.longitude;

        // Sensor enabled
        String sensor = SENSOR_FALSE;

        // Waypoints
        //String waypoints = "";
        String waypoints = "";
        if(markerPoints.size()>2) {
            for (int i = 1; i < markerPoints.size(); i++) {
                LatLang point = markerPoints.get(i);
                if (i == 1) {
                    waypoints = WAYPOINTS_VIA;
                }
                if (i != markerPoints.size() - 1) {
                    waypoints += point.getLatitude() + "," + point.getLongitude() + "|";
                }
            }
        }
        String parameters = str_origin+"&"+str_dest+"&"+sensor+"&"+waypoints;



        // Output format
        String output =TYPE_JSON ;

        // Building the url to the web service
        String url = DIRECTION_URL+output+"?"+parameters;

        Logger.e("waypoints",url);

        return url;
    }

    public double calculateAngle(LatLang startPoint, LatLang endPoint) {
        double lat1 = startPoint.getLatitude() / 1E6;
        double lat2 = endPoint.getLatitude() / 1E6;
        double long2 = startPoint.getLongitude() / 1E6;
        double long1 = endPoint.getLongitude() / 1E6;
        double dy = lat2 - lat1;
        double dx = Math.cos(Math.PI / 180 * lat1) * (long2 - long1);
        double angle = Math.atan2(dy, dx);
        return angle;
    }

}
