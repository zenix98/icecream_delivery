package com.cushmantracking.app.cushman.dagger;


import com.cushmantracking.app.cushman.ui.driver.activities.BaseActivity;
import com.cushmantracking.app.cushman.ui.driver.fragments.BaseFragment;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by amitRai on 17/04/2017.
 * to inject dependencies in activities and fragments.
 */
@Singleton
@Component(modules={Modules.class})
public interface Injector {
     void inject(BaseFragment fragment);
     void inject(BaseActivity activity);
}

