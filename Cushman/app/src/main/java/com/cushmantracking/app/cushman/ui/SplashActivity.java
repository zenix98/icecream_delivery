package com.cushmantracking.app.cushman.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.cushmantracking.app.cushman.R;
import com.cushmantracking.app.cushman.ui.driver.activities.BaseActivity;

public class SplashActivity extends AppCompatActivity {

    private Context context = null;
    private int splash_timeout = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        init();
    }

    /**
     * initialize view elements.
     */
    private void init(){
        context = this;
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                if (context != null){
                    Intent mInHome = new Intent(SplashActivity.this, BaseActivity.class);
                    SplashActivity.this.startActivity(mInHome);
                    SplashActivity.this.finish();
                }
            }
        }, splash_timeout);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        context = null;
    }
}
