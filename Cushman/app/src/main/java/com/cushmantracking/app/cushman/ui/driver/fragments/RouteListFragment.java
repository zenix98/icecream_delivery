package com.cushmantracking.app.cushman.ui.driver.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.cushmantracking.app.cushman.R;
import com.cushmantracking.app.cushman.bean.BaseBean;
import com.cushmantracking.app.cushman.bean.DriverRouteBean;
import com.cushmantracking.app.cushman.bean.RouteResponseBean;
import com.cushmantracking.app.cushman.listeners.ApiCallback;
import com.cushmantracking.app.cushman.ui.driver.adapters.RouteListAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class RouteListFragment extends BaseFragment implements View.OnClickListener{

    private RecyclerView recycle_view_route = null;
    private ImageView img_add = null;
    private final String TAG = getClass().getSimpleName();
    private List<DriverRouteBean> list_routes = new ArrayList<>();
    private RouteListAdapter adapter_route_list = null;

    private ProgressDialog pd = null;



    public RouteListFragment() {
        // Required empty public constructor
    }

    @Override
    public void initView(View view) {
        list_routes.clear();
        adapter_route_list = new RouteListAdapter(list_routes, getActivity());
        recycle_view_route = (RecyclerView) view.findViewById(R.id.recycle_view_route);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recycle_view_route.setLayoutManager(linearLayoutManager);
        recycle_view_route.setAdapter(adapter_route_list);

        img_add = (ImageView) view.findViewById(R.id.img_add);
        img_add.setOnClickListener(this);

        pd = new ProgressDialog(getContext());
        pd.setMessage("Please wait, getting your routes");

        getRoutes();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_route_list, container, false);
        initView(view);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_add:
                replaceFragment(new CreateRouteFragment(), true);
                break;
        }
    }

    private void getRoutes() {
        if (pd != null && !pd.isShowing())
            pd.show();
        routeRequest.getAllRoutes(preference.getToken(), new ApiCallback() {
            @Override
            public void onRequestSuccess(BaseBean bean) {
                Log.e(TAG, ""+bean);
                if (pd != null && pd.isShowing())
                    pd.dismiss();
                RouteResponseBean routeModal = (RouteResponseBean) bean;
                list_routes.addAll(routeModal.getData());
                adapter_route_list.notifyDataSetChanged();
            }

            @Override
            public void onRequestFailed(String message) {
                if (pd != null && pd.isShowing())
                    pd.dismiss();
                Log.e(TAG, ""+message);
                utility.showSnackBar(activity.layout_snackbar, ""+message);
            }
        });
    }
}
