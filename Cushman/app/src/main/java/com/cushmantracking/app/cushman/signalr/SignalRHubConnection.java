//package com.cushmantracking.app.cushman.services;
//
//import android.content.Context;
//import android.util.Log;
//
//import com.bitcompute.twingle.R;
//import com.bitcompute.twingle.http.HttpConstant;
//import com.bitcompute.twingle.utils.AppPreferences;
//import com.bitcompute.twingle.utils.AppUtility;
//import com.bitcompute.twingle.utils.PrefConstants;
//import com.google.gson.Gson;
//
//import org.json.JSONArray;
//import org.json.JSONObject;
//
//import java.util.concurrent.ExecutionException;
//
//import microsoft.aspnet.signalr.client.Credentials;
//import microsoft.aspnet.signalr.client.Platform;
//import microsoft.aspnet.signalr.client.SignalRFuture;
//import microsoft.aspnet.signalr.client.http.Request;
//import microsoft.aspnet.signalr.client.http.android.AndroidPlatformComponent;
//import microsoft.aspnet.signalr.client.hubs.HubConnection;
//import microsoft.aspnet.signalr.client.hubs.HubProxy;
//import microsoft.aspnet.signalr.client.hubs.SubscriptionHandler1;
//import microsoft.aspnet.signalr.client.transport.ClientTransport;
//import microsoft.aspnet.signalr.client.transport.ServerSentEventsTransport;
//
///**
// * Created by arvind on 24/11/15.
// */
//public class SignalRHubConnection {
//    // HubConnection
//    public static HubConnection mHubConnection;
//    public static HubProxy mHubProxy;
//    private static Context context;
//    public static String mConnectionID;
//
//    public SignalRHubConnection(Context context) {
//        this.context = context;
//    }
//
//    //
//     /*
//    This function try to connect with Twingle chat hub and return connection ID.
//     */
//    public static void startSignalR() {
//        Log.d("Start SignalR ", "############");
//        if (AppUtility.isInternetAvailable(context) == false) {
//
//            //AppUtility.showAlertDialog(context, context.getString(R.string.app_name), context.getString(R.string.connection_error));
//
//        } else {
//            try {
//                Platform.loadPlatformComponent(new AndroidPlatformComponent());
//
//                Credentials credentials = new Credentials() {
//                    @Override
//                    public void prepareRequest(Request request) {
//                        AppPreferences appPrefs = AppPreferences.getInstance(context);
//                        request.addHeader("UserName", appPrefs.getString(PrefConstants.USER_NAME));
//                    }
//                };
//                mHubConnection = new HubConnection(HttpConstant.TWINGLE_CHAT_HUB_URL);
//                mHubConnection.setCredentials(credentials);
//                mHubProxy = mHubConnection.createHubProxy(HttpConstant.TWINGLE_CHAT_HUB);
//                ClientTransport clientTransport = new ServerSentEventsTransport(mHubConnection.getLogger());
//                SignalRFuture<Void> signalRFuture = mHubConnection.start(clientTransport);
//                signalRFuture.get();
//                //set connection id
//                mConnectionID = mHubConnection.getConnectionId();
//
//                // To get onLine user list
//                mHubProxy.on("onGetOnlineContacts",
//                        new SubscriptionHandler1<Object>() {
//                            @Override
//                            public void run(final Object msg) {
//
//                                try {
//                                    Gson gson = new Gson();
//                                    String json = gson.toJson(msg);
//                                    JSONObject responseObject = new JSONObject(json.toString());
//                                    JSONArray jsonArray = responseObject.getJSONArray("messageRecipients");
//                                    int sizeOfList = jsonArray.length();
//                                    for (int i = 0; i < sizeOfList; i++) {
//
//                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
//                                        JSONArray onLineUserList = jsonObject.getJSONArray("TwingleChatGroupIds");
//                                        int onLineUserCount = onLineUserList.length();
//
//                                    }
//
//
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }
//
//
//                            }
//                        }
//                        , Object.class);
//
//
//            } catch (InterruptedException | ExecutionException e) {
//                e.printStackTrace();
//
//
//            }
//
//        }
//
//    }
//}
