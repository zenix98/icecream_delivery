package com.cushmantracking.app.cushman.signalr;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import com.cushmantracking.app.cushman.db.AppPreference;
import com.cushmantracking.app.cushman.util.Logger;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.concurrent.ExecutionException;

import microsoft.aspnet.signalr.client.Action;
import microsoft.aspnet.signalr.client.Credentials;
import microsoft.aspnet.signalr.client.ErrorCallback;
import microsoft.aspnet.signalr.client.MessageReceivedHandler;
import microsoft.aspnet.signalr.client.Platform;
import microsoft.aspnet.signalr.client.SignalRFuture;
import microsoft.aspnet.signalr.client.http.Request;
import microsoft.aspnet.signalr.client.http.android.AndroidPlatformComponent;
import microsoft.aspnet.signalr.client.hubs.HubConnection;
import microsoft.aspnet.signalr.client.hubs.HubProxy;
import microsoft.aspnet.signalr.client.transport.ClientTransport;
import microsoft.aspnet.signalr.client.transport.ServerSentEventsTransport;

/**
 * Created by amitrai on 31/5/17.
 * handling signal r services.
 */

public class SignalRService extends Service{

    private static final String TAG = "Service";
    private static final String SERVER_HUB_NAME = "StockUpdateHub";
    private HubConnection mHubConnection;
    private static HubProxy mHubProxy;
    private Handler mHandler; // to display Toast message
    private final IBinder mBinder = new LocalBinder();
    private final static String serverUrl = "http://192.168.1.14:7000/";
    
    private final static String SERVER_HUB_STOCK_UPDATE = "SingleParam";
    private final static String SERVER_METHOD_RECEIVE = "NotifyFromSingleParam";

    private AppPreference sp;

    @Override
    public void onCreate() {
        super.onCreate();

        Logger.e(TAG, "Service Created");

        sp = new AppPreference(this);
        mHandler = new Handler(Looper.myLooper());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        int result = super.onStartCommand(intent, flags, startId);
        startSignalR();
        return result;
    }

    @Override
    public IBinder onBind(Intent intent) {

        startSignalR();
        return mBinder;
    }

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        public SignalRService getService() {
            // Return this instance of SignalRService so clients can call public methods
            return SignalRService.this;
        }
    }

    /**
     * method for clients (activities)
     */
    public static void sendMessage(String message, float latitude, String longitude) {

        mHubProxy.invoke(SERVER_HUB_STOCK_UPDATE, message).done(new Action<Void>() {
            @Override
            public void run(Void aVoid) throws Exception {
                Logger.e(TAG, "acknowledged "+aVoid);
            }
        }).onError(new ErrorCallback() {
            @Override
            public void onError(Throwable throwable) {
                Logger.e(TAG,"an error occurred");
            }
        });
    }


    @Override
    public void onDestroy() {

        mHubConnection.stop();
        super.onDestroy();

    }


    public void startSignalR() {
        Log.d("Start SignalR ", "############");
        try {
            Platform.loadPlatformComponent(new AndroidPlatformComponent());

            Credentials credentials = new Credentials() {
                @Override
                public void prepareRequest(Request request) {
//                    AppPreferences appPrefs = AppPreferences.getInstance(context);
//                    request.addHeader("UserName", appPrefs.getString(PrefConstants.USER_NAME));
                }
            };
            mHubConnection = new HubConnection(serverUrl);
            mHubConnection.setCredentials(credentials);
            mHubProxy = mHubConnection.createHubProxy(SERVER_HUB_NAME);
            ClientTransport clientTransport = new ServerSentEventsTransport(mHubConnection.getLogger());
            SignalRFuture<Void> signalRFuture = mHubConnection.start(clientTransport);
            signalRFuture.get();
            //set connection id
            String mConnectionID = mHubConnection.getConnectionId();
            Log.e(TAG, "connetion id : "+mConnectionID);


//            mHubProxy.subscribe(this);
//
//
//            mHubProxy.on(SERVER_METHOD_RECEIVE, new SubscriptionHandler() {
//                @Override
//                public void run() {
//                    Log.e("received something", "TAG");
//                }
//            });
//            SubscriptionHandler1 subscriptionHandler1= new SubscriptionHandler1<String>() {
//                @Override
//                public void run(final String msg) {
//                    final String finalMsg = msg;
//                    Log.e(TAG, "" + msg);
//                    // display Toast message
//                    mHandler.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            Toast.makeText(getApplicationContext(), finalMsg, Toast.LENGTH_SHORT).show();
//                        }
//                    });
//                }
//            };
//
//            mHubProxy.on(SERVER_METHOD_RECEIVE,subscriptionHandler1
//
//                    , String.class);
//
//
//            mHubProxy.on(SERVER_METHOD_RECEIVE,
//                    new SubscriptionHandler1<String>() {
//                        @Override
//                        public void run(final String msg) {
//                            final String finalMsg = msg;
//                            Log.e(TAG, ""+msg);
//                            // display Toast message
//                            mHandler.post(new Runnable() {
//                                @Override
//                                public void run() {
//                                    Toast.makeText(getApplicationContext(), finalMsg, Toast.LENGTH_SHORT).show();
//                                }
//                            });
//                        }
//                    }
//                    , String.class);
//
//
//            mHubProxy.on(SERVER_METHOD_RECEIVE,
//                    new SubscriptionHandler1<Object>() {
//                        @Override
//                        public void run(final Object msg) {
//
//                            try {
////                                Gson gson = new Gson();
////                                String json = gson.toJson(msg);
////                                JSONObject responseObject = new JSONObject(json.toString());
////                                JSONArray jsonArray = responseObject.getJSONArray("messageRecipients");
////                                int sizeOfList = jsonArray.length();
////                                for (int i = 0; i < sizeOfList; i++) {
////
////                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
////                                    JSONArray onLineUserList = jsonObject.getJSONArray("TwingleChatGroupIds");
////                                    int onLineUserCount = onLineUserList.length();
//
//                                Log.e(TAG, "message received");
//
////                                }
//
//
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//
//
//                        }
//                    }
//                    , Object.class);
//
//            mHubProxy.subscribe(subscriptionHandler1);


//
//
//

            mHubProxy.subscribe(this);

//            clientTransport = new LongPollingTransport(mHubConnection.getLogger());

        /* ****new codes here**** */
//            mHubConnection.start(clientTransport);

        /* ****new codes here**** */
        /* ****seems useless but should be here!**** */
            mHubProxy.subscribe(new Object() {
                @SuppressWarnings("unused")
                public void newMessage(final String message, final String messageId, final String chatId,
                                       final String senderUserId, final String fileUrl, final String replyToMessageId) {

                    Log.e(TAG, "new message");

                }
            });


        /* ********************** */
        /* ****new codes here**** */
        /* **** the main method that I fetch data from server**** */
            mHubConnection.received(new MessageReceivedHandler() {
                @Override
                public void onMessageReceived(final JsonElement json) {
                    JsonObject jsonObject = json.getAsJsonObject();
                    Log.e("<Debug>", "response = " + jsonObject.toString());
                }
            });

        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

    }

}
