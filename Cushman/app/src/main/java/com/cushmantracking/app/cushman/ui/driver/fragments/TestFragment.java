package com.cushmantracking.app.cushman.ui.driver.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.cushmantracking.app.cushman.R;
import com.cushmantracking.app.cushman.signalr.SignalRService;
import com.cushmantracking.app.cushman.util.Logger;

/**
 * A simple {@link Fragment} subclass.
 */
public class TestFragment extends BaseFragment implements View.OnClickListener{

    private EditText edt_message = null;
    private Button btn_send = null;

    private final String TAG = getClass().getSimpleName();


    public TestFragment() {
        // Required empty public constructor
    }

    @Override
    public void initView(View view) {
        edt_message = (EditText) view.findViewById(R.id.edt_message);
        btn_send = (Button) view.findViewById(R.id.btn_send);
        btn_send.setOnClickListener(this);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Logger.e(TAG, "on create");
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_test, container, false);
        initView(view);
        return view;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_send){
            String message = edt_message.getText().toString().trim();
            if (message.length() >0){
                SignalRService.sendMessage(message, 48, "its a bright day");
                edt_message.setText("");
                utility.showSnackBar(activity.layout_snackbar, "message sent");
            }
            else
                utility.showSnackBar(activity.layout_snackbar, "Can not send empty message");
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        Logger.e(TAG, "on start");
        getActivity().startService(new Intent(getContext(), SignalRService.class));
    }





}
