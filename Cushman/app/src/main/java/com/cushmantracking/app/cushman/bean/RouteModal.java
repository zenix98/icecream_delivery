package com.cushmantracking.app.cushman.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by amitrai on 13/6/17.
 * modal for route.
 */

public class RouteModal extends BaseBean {

    private String AccessToken;
    private List<LatLang> Route;


    @SerializedName("DriverRouteID")
    @Expose
    private Integer DriverRouteID;

    @SerializedName("RouteName")
    @Expose
    private String RouteName;

    public Integer getDriverRouteID() {
        return DriverRouteID;
    }

    public void setDriverRouteID(Integer DriverRouteID) {
        this.DriverRouteID = DriverRouteID;
    }

    public String getRouteName() {
        return RouteName;
    }

    public void setRouteName(String RouteName) {
        this.RouteName = RouteName;
    }

    public RouteModal(String AccessToken, String RouteName,
                      List<LatLang> Route){
        this.AccessToken =AccessToken;
        this.RouteName = RouteName;
        this.Route = Route;
    }


    public String getAccessToken() {
        return AccessToken;
    }

    public void setAccessToken(String accessToken) {
        AccessToken = accessToken;
    }


    public List<LatLang> getRoute() {
        return Route;
    }

    public void setRoute(List<LatLang> Route) {
        this.Route = Route;
    }
}
