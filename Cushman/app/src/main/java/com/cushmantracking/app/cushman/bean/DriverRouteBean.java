package com.cushmantracking.app.cushman.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by amitrai on 13/6/17.
 * bean class for route data
 */

public class DriverRouteBean extends BaseBean {


    @SerializedName("DriverRouteID")
    @Expose
    private Integer driverRouteID;
    @SerializedName("RouteName")
    @Expose
    private String routeName;

    public Integer getDriverRouteID() {
        return driverRouteID;
    }

    public void setDriverRouteID(Integer driverRouteID) {
        this.driverRouteID = driverRouteID;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }
}
