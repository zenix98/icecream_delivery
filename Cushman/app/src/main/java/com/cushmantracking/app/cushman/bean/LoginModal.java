package com.cushmantracking.app.cushman.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by amitrai on 12/6/17.
 * login modal
 */

public class LoginModal extends BaseBean{
    String DeviceID, DeviceToken, DeviceType, Email, Password;

    public LoginModal(String DeviceID, String DeviceToken, String DeviceType, String Email, String Password){
        this.DeviceID = DeviceID;
        this.DeviceType = DeviceType;
        this.Email = Email;
        this.Password = Password;
        this.DeviceToken = DeviceToken;
    }


    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("AccessToken")
    @Expose
    private String accessToken;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getDeviceID() {
        return DeviceID;
    }

    public void setDeviceID(String deviceID) {
        DeviceID = deviceID;
    }

    public String getDeviceToken() {
        return DeviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        DeviceToken = deviceToken;
    }

    public String getDeviceType() {
        return DeviceType;
    }

    public void setDeviceType(String deviceType) {
        DeviceType = deviceType;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }
}
