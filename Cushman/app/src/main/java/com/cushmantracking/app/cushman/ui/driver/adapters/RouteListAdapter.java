package com.cushmantracking.app.cushman.ui.driver.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.cushmantracking.app.cushman.R;
import com.cushmantracking.app.cushman.bean.DriverRouteBean;
import com.cushmantracking.app.cushman.ui.driver.activities.BaseActivity;
import com.cushmantracking.app.cushman.ui.driver.fragments.StartRouteFragment;
import com.cushmantracking.app.cushman.util.CustomTextView;
import com.cushmantracking.app.cushman.util.Logger;

import java.util.List;

/**
 * Created by amitrai on 14/6/17.
 * User for :- inflating data into recycler view for route list.
 */

public class RouteListAdapter extends RecyclerView.Adapter<RouteListAdapter.RouteView>{

    private List<DriverRouteBean> routeModalList;
    private Activity activity;
    private final String TAG = getClass().getSimpleName();

    public RouteListAdapter(List<DriverRouteBean> routeModalList, Activity activity){
        this.routeModalList = routeModalList;
        this.activity = activity;
    }


    @Override
    public RouteView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_route,
                parent, false);
        return new RouteView(view);
    }

    @Override
    public void onBindViewHolder(RouteView holder, final int position) {
        DriverRouteBean routeModal = routeModalList.get(position);
        if (routeModal != null){
            holder.txt_route_name.setText(routeModal.getRouteName());
            holder.img_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Logger.e(TAG, "edit clicked");
                }
            });

            holder.img_go.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Logger.e(TAG, "done clicked");
                    if (activity instanceof BaseActivity){
                        BaseActivity act = (BaseActivity) activity;
                        StartRouteFragment.route_id = routeModalList.get(position).getDriverRouteID();
                        act.replaceFragment(new StartRouteFragment(), true);
                    }

                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return routeModalList.size();
    }

    class RouteView extends RecyclerView.ViewHolder{
        CustomTextView txt_route_name;
        ImageView img_edit, img_go;
        public RouteView(View itemView) {
            super(itemView);
            img_edit = (ImageView) itemView.findViewById(R.id.img_edit);
            img_go  = (ImageView) itemView.findViewById(R.id.img_go);
            txt_route_name  = (CustomTextView) itemView.findViewById(R.id.txt_route_name);
        }
    }
}
