package com.cushmantracking.app.cushman.ui.driver.fragments;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.View;

import com.cushmantracking.app.cushman.AppInitials;
import com.cushmantracking.app.cushman.db.AppPreference;
import com.cushmantracking.app.cushman.http.ApiInterface;
import com.cushmantracking.app.cushman.http.ConnectionManager;
import com.cushmantracking.app.cushman.http.request.LoginRequest;
import com.cushmantracking.app.cushman.http.request.NotificationRequest;
import com.cushmantracking.app.cushman.http.request.RouteRequest;
import com.cushmantracking.app.cushman.ui.driver.activities.BaseActivity;
import com.cushmantracking.app.cushman.util.AppConstants;
import com.cushmantracking.app.cushman.util.Utility;
import com.google.gson.Gson;

import javax.inject.Inject;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BaseFragment} factory method to
 * create an instance of this fragment.
 */
public abstract class BaseFragment extends Fragment {

    @Inject
    ApiInterface service;

    @Inject
    Utility utility;

    @Inject
    Gson gson;

    @Inject
    ConnectionManager connectionManager;

    @Inject
    AppConstants constants;


    @Inject
    NotificationRequest notificationRequest;

    @Inject
    LoginRequest loginRequest;

    @Inject
    RouteRequest routeRequest;

    @Inject
    AppPreference preference;

    BaseActivity activity;

    public BaseFragment() {
        // Required empty public constructor
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        AppInitials.getContext().getInjector().inject(this);

        if (getActivity() instanceof  BaseActivity)
            activity = (BaseActivity) getActivity();
    }

    /**
     * initialize view elements here
     * @param view parent view of fragment
     */
    public abstract void initView(View view);

    public void replaceFragment(BaseFragment fragment, boolean isAddToStack){
        activity.replaceFragment(fragment, isAddToStack);
    }



}
