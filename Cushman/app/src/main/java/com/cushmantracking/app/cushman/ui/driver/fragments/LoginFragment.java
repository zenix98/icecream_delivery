package com.cushmantracking.app.cushman.ui.driver.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.cushmantracking.app.cushman.R;
import com.cushmantracking.app.cushman.bean.BaseBean;
import com.cushmantracking.app.cushman.bean.LoginModal;
import com.cushmantracking.app.cushman.listeners.ApiCallback;
import com.cushmantracking.app.cushman.util.AppConstants;
import com.cushmantracking.app.cushman.util.CustomTextView;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends BaseFragment implements View.OnClickListener{

    private CustomTextView txt_login;
    private EditText edt_username = null, edt_password = null;
    private String TAG = getClass().getSimpleName();

    private ProgressDialog pd;


    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view  =  inflater.inflate(R.layout.fragment_login, container, false);
        initView(view);
        return view;
    }


    @Override
    public void initView(View view) {
        txt_login = (CustomTextView) view.findViewById(R.id.txt_login);
        edt_password = (EditText) view.findViewById(R.id.edt_password);
        edt_username = (EditText) view.findViewById(R.id.edt_username);
        txt_login.setOnClickListener(this);

        pd = new ProgressDialog(getContext());
        pd.setMessage("please wait while logging you in.");
        pd.setCancelable(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.txt_login:
                attemptLogin();
                break;
        }
    }

    /**
     * attempts login
     */
    private void attemptLogin(){
        String username, password;
        username = edt_username.getText().toString().trim();
        password = edt_password.getText().toString().trim();
        boolean cancel = false;
        View focus_view = null;
        LoginModal modal = null;
        if (username.isEmpty()){
            edt_username.setError("Can't be left blank");
            focus_view = edt_username;
        }else if (password.isEmpty()){
            edt_password.setError("Can't be left blank");
            focus_view = edt_password;


        }

        if (!cancel){
            if (pd != null && !pd.isShowing())
                pd.show();
            modal = new LoginModal("asdf",
                    AppConstants.FCM_TOKEN,
                    AppConstants.DEVICE_TYPE_ANDROID,
                    username, password);

               loginRequest.attemptLoginDriver(modal, new ApiCallback() {
                   @Override
                   public void onRequestSuccess(BaseBean bean) {
                       if (pd != null && pd.isShowing())
                           pd.dismiss();
                       Log.e(TAG, ""+bean);
                       LoginModal loginModal = (LoginModal) bean;
                       preference.saveDriverInfo(loginModal);

                       replaceFragment(new RouteListFragment(), false);

                   }

                   @Override
                   public void onRequestFailed(String message) {
                        Log.e(TAG, "login failed "+message);
                       if (pd != null && pd.isShowing())
                           pd.dismiss();
                       utility.showSnackBar(activity.layout_snackbar, ""+message);
                   }
               });
        }

    }
}
