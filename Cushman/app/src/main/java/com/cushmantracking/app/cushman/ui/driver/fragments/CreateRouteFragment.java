package com.cushmantracking.app.cushman.ui.driver.fragments;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;

import com.cushmantracking.app.cushman.R;
import com.cushmantracking.app.cushman.bean.BaseBean;
import com.cushmantracking.app.cushman.bean.LatLang;
import com.cushmantracking.app.cushman.bean.PolyBean;
import com.cushmantracking.app.cushman.bean.RouteModal;
import com.cushmantracking.app.cushman.listeners.ApiCallback;
import com.cushmantracking.app.cushman.util.AppConstants;
import com.cushmantracking.app.cushman.util.DirectionsJSONParser;
import com.cushmantracking.app.cushman.util.Logger;
import com.cushmantracking.app.cushman.util.Utility;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link CreateRouteFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CreateRouteFragment extends BaseFragment implements OnMapReadyCallback,
        GoogleMap.OnMapClickListener, GoogleMap.OnMarkerClickListener, View.OnClickListener{
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private List<Marker> list_marker = new ArrayList<>();
    private List<PolyBean> hash_polylines = new ArrayList<>();

    private String TAG = getClass().getSimpleName();

    MapView mMapView;
    GoogleMap map;

    private ProgressDialog pd;

    private String mParam1;
    private String mParam2;
    private Marker poly_marker;

    private Button btn_save_route = null;
    String rounte_name = null;


    public CreateRouteFragment() {
        // Required empty public constructor
        AppConstants.FCM_TOKEN = FirebaseInstanceId.getInstance().getToken();
        Logger.e(TAG, ""+AppConstants.FCM_TOKEN);
    }

    @Override
    public void initView(View view) {
        list_marker.clear();
        hash_polylines.clear();
        pd = new ProgressDialog(getContext());
        pd.setMessage("please wait while creating your route");
        pd.setCancelable(false);
        btn_save_route = (Button) view.findViewById(R.id.btn_save_route);
        btn_save_route.setOnClickListener(this);

//        SmartLocation.with(getContext()).location()
//                .start(new OnLocationUpdatedListener() {
//                    @Override
//                    public void onLocationUpdated(Location locat) {
//                        if (locat != null)
//                            map.moveCamera(CameraUpdateFactory
//                                    .newLatLngZoom(new LatLng(locat.getLatitude(), locat.getLongitude()), 14));
//                    }
//                });



    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CreateRouteFragment.
     */
    public static CreateRouteFragment newInstance(String param1, String param2) {
        CreateRouteFragment fragment = new CreateRouteFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_create_route, container, false);
        initView(v);
        mMapView = (MapView) v.findViewById(R.id.mapview);
        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(this); //this is important

        return v;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setOnMapClickListener(this);

    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onMapClick(LatLng latLng) {

        if (Utility.isInternetAvailable(getContext())){
            map.getUiSettings().setZoomControlsEnabled(true);
            Marker marker = map.addMarker(new MarkerOptions().position(latLng));

            list_marker.add(marker);
            map.setOnMarkerClickListener(this);
            if (list_marker.size() > 1)
                createWayPoint(marker);
            else
                hash_polylines.add(new PolyBean(marker, null));
        }else
            utility.showSnackBar(activity.layout_snackbar, "No Internet connection.");

        btn_save_route.setVisibility(list_marker.size() > 1 ? View.VISIBLE : View.GONE);
//        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10));
    }

    @Override
    public boolean onMarkerClick(Marker marker) {


        if (list_marker.size() >2 && marker.getId().equalsIgnoreCase(list_marker.get(0).getId())){
            return false;
        }else {
            for (int i =0; i < hash_polylines.size(); i++){
                Marker polyMarker = hash_polylines.get(i).getMarker();
                if (polyMarker != null){
                    if (marker.getId().equals(polyMarker.getId())){
                        clearPolyline(i);
                        hash_polylines.subList(i, hash_polylines.size()).clear();
                    }
                }
            }

            for (int i =0; i < list_marker.size(); i++){
                if (marker.getId().equals(list_marker.get(i).getId())){
                    clearMarkers(i);
                    list_marker.subList(i, list_marker.size()).clear();
                }
            }
            marker.remove();
            return false;
        }

    }


    private void clearMarkers(int afterPosition){
        for (int i = list_marker.size(); i > afterPosition ; i--){
            list_marker.get(i-1).remove();
        }
    }


    private void clearPolyline(int afterPosition){
        for (int i = hash_polylines.size(); i > afterPosition ; i--){
            Polyline polyline = hash_polylines.get(i - 1).getPolyline();
            if (polyline != null)
                polyline.remove();
        }
    }


    /**
     * creates new way points from the previous marker to the new marker
     */
    private void createWayPoint(Marker marker){
        poly_marker = marker;
        LatLng origin = list_marker.get(list_marker.size()-2).getPosition();
        LatLng destination = marker.getPosition();
        String url = getDirectionsUrl(origin, destination);

        Log.e(TAG, "create route "+url);

        if (pd != null && !pd.isShowing() && isAdded())
            pd.show();

        DownloadTask downloadTask = new DownloadTask();

        // Start downloading json data from Google Directions API
        downloadTask.execute(url);
    }




    private class DownloadTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            String data = "";

            try {
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();


            parserTask.execute(result);

        }
    }


    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
                if (pd != null && pd.isShowing() && isAdded())
                    pd.dismiss();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList();
                lineOptions = new PolylineOptions();

                List<HashMap<String, String>> path = result.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);
                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                lineOptions.addAll(points);
                lineOptions.width(10);
                lineOptions.color(Color.BLUE);
                lineOptions.geodesic(true);

            }

// Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null){
                Polyline line = map.addPolyline(lineOptions);
                PolyBean bean = new PolyBean(poly_marker, line);
                hash_polylines.add(bean);
            }

            if (pd != null && pd.isShowing() && isAdded())
                pd.dismiss();
        }
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";
        String mode = "mode=driving";
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.connect();

            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }








    public void rotateMarker(final Marker marker, final float toRotation, final float st) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final float startRotation = st;
        final long duration = 1555;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed / duration);

                float rot = t * toRotation + (1 - t) * startRotation;

                marker.setRotation(-rot > 180 ? rot / 2 : rot);
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
            }
        });
    }


    public void animateMarker(final Marker m, final LatLng toPosition, final boolean hideMarke) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = map.getProjection();
        Point startPoint = proj.toScreenLocation(m.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 5000;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                m.setPosition(new LatLng(lat, lng));

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else
                    m.setVisible(!hideMarke);


            }
        });
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_save_route:
                saveRoute();
                break;
        }
    }


    private void saveRoute(){
        if (list_marker.size()>1){
            LayoutInflater factory = LayoutInflater.from(getContext());
            final View deleteDialogView = factory.inflate(R.layout.dialog_save_route, null);
            final AlertDialog deleteDialog = new AlertDialog.Builder(getContext()).create();
            deleteDialog.setView(deleteDialogView);


            deleteDialogView.findViewById(R.id.btn_save).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EditText edt_route_name = (EditText) deleteDialogView.findViewById(R.id.edt_route_name);
                    rounte_name = edt_route_name.getText().toString().trim();
                    if (!rounte_name.isEmpty()){

                        if (pd != null && !pd.isShowing())
                            pd.show();

                        List<LatLang> list_latlan = new ArrayList<LatLang>();
                        for (Marker marker: list_marker){
                            list_latlan.add(new LatLang(marker.getPosition().latitude, marker.getPosition().longitude));
                        }

                        RouteModal routeModal = new RouteModal(preference.getToken(),
                                rounte_name, list_latlan);
                        routeRequest.saveRoute(routeModal, new ApiCallback() {
                            @Override
                            public void onRequestSuccess(BaseBean bean) {
                                if (pd != null && pd.isShowing())
                                    pd.dismiss();

                                getActivity().onBackPressed();
                            }

                            @Override
                            public void onRequestFailed(String message) {

                                if (pd != null && pd.isShowing())
                                    pd.dismiss();
                            }
                        });

                        deleteDialog.dismiss();
                    }else
                        edt_route_name.setError(getString(R.string.name_required));
                }
            });
            deleteDialogView.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteDialog.dismiss();
                }
            });

            deleteDialog.show();
        }else
            utility.showSnackBar(activity.layout_snackbar, "Create your route first!");
    }




    @Override
    public void onStart() {
        super.onStart();
    }
}
