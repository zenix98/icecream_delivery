package com.cushmantracking.app.cushman.bean;

/**
 * Created by amitrai on 13/6/17.
 * modal for storing latitude and longitude
 */

public class LatLang {
    private double Latitude, Longitude;

    public LatLang(double Latitude, double Longitude){
        this.Latitude = Latitude;
        this.Longitude = Longitude;
    }

    public double getLatitude() {
        return Latitude;
    }

    public void setLatitude(double latitude) {
        Latitude = latitude;
    }

    public double getLongitude() {
        return Longitude;
    }

    public void setLongitude(double longitude) {
        Longitude = longitude;
    }
}
