package com.cushmantracking.app.cushman.http.request;

/**
 * Created by amitrai on 1/5/17.
 * this class is used for :- to create all the requests in the app.
 */

public class NotificationRequest extends BaseRequest {

    private final String TAG = getClass().getSimpleName();
    /**
     * gets all the notification for the device
     * @param callback for communicating to the calling function
     */
//    public void getAllNotifications(String page, String device_id, final ApiCallback callback){
//        Call<ResponseBody> callNotication=getAPIClient().getNotifications(page, device_id);
//        ConnectionManager connectionManager= ConnectionManager.getConnectionInstance(callNotication);
//        connectionManager.callApi(new BaseListener.OnWebServiceCompleteListener() {
//            @Override
//            public void onWebServiceComplete(ResponseBody responseBody) {
//                try {
//                    JSONObject obj=new JSONObject(responseBody.string());
//                    String status = obj.getString(STATUS);
//                    String message=obj.getString(MESSAGE);
//                    if (status.equalsIgnoreCase("success")) {
//                        String data = obj.getJSONObject(DATA).toString();
//                        Data bean = getGsonBuilder().fromJson(data, Data.class);
//                        callback.onRequestSuccess(bean);
//                    } else {
//                        callback.onRequestFailed(message);
//                    }
//                }catch (Exception exp){
//                    exp.printStackTrace();
//                    if (exp.getMessage() != null)
//                        callback.onRequestFailed(exp.getMessage());
//                    else
//                        callback.onRequestFailed(""+exp);
//                }
//            }
//
//            @Override
//            public void onWebStatusFalse(String message) {
//                callback.onRequestFailed(""+message);
//            }
//        });
//    }


    /**
     * fetches all the unread notifications from the server.
     * @param callback to propel response to the requester.
     */
//    public void getNotificationCount(String device_id, final ApiCallback callback){
//        Call<ResponseBody> callNotication=getAPIClient().getNotificationCount(device_id);
//        ConnectionManager connectionManager=ConnectionManager.getConnectionInstance(callNotication);
//        connectionManager.callApi(new BaseListener.OnWebServiceCompleteListener() {
//            @Override
//            public void onWebServiceComplete(ResponseBody responseBody) {
//                try {
//                    assert responseBody != null;
//                    JSONObject obj;
//                    obj = new JSONObject(responseBody.string());
//                    String status = obj.getString(STATUS);
//                    String message=obj.getString(MESSAGE);
//                    if (status.equalsIgnoreCase("success")) {
//                        String data = obj.getJSONObject(DATA).toString();
//                        Log.e(TAG, "notification count "+data);
//                        NotificationCountModal bean = getGsonBuilder().fromJson(data, NotificationCountModal.class);
//                        callback.onRequestSuccess(bean);
//                    } else {
//                        callback.onRequestFailed(message);
//                    }
//                }catch (Exception exp){
//                    exp.printStackTrace();
//                    if (exp.getMessage() != null)
//                        callback.onRequestFailed(exp.getMessage());
//                    else
//                        callback.onRequestFailed(""+exp);
//                }
//            }
//
//            @Override
//            public void onWebStatusFalse(String message) {
//                callback.onRequestFailed(""+message);
//            }
//        });
//
//    }

    /**
     * saves fcm token on server
     * @param token to be saved
     * @param callback to propel response to the requester
     */
//    public void saveToken(String device_id, String token, final ApiCallback callback){
//        Call<ResponseBody> callNotication=getAPIClient().saveFcmToken(device_id, token,
//                AppConstants.DEVICE_TYPE_ANDROID);
//        ConnectionManager connectionManager=ConnectionManager.getConnectionInstance(callNotication);
//        connectionManager.callApi(new BaseListener.OnWebServiceCompleteListener() {
//            @Override
//            public void onWebServiceComplete(ResponseBody responseBody) {
//                try {
//                    JSONObject obj=new JSONObject(responseBody.string());
//                    String status = obj.getString(STATUS);
//                    String message=obj.getString(MESSAGE);
//                    if (status.equalsIgnoreCase("success")) {
//                        BaseBean bean = new BaseBean();
//                        bean.setMessage(message);
//                        //bean.setStatus(status);
//                        callback.onRequestSuccess(bean);
//                    } else {
//                        callback.onRequestFailed(message);
//                    }
//                }catch (Exception exp){
//                    exp.printStackTrace();
//                    if (exp.getMessage() != null)
//                        callback.onRequestFailed(exp.getMessage());
//                    else
//                        callback.onRequestFailed(""+exp);
//                }
//            }
//
//            @Override
//            public void onWebStatusFalse(String message) {
//                callback.onRequestFailed(""+message);
//            }
//        });
//
//    }

}
