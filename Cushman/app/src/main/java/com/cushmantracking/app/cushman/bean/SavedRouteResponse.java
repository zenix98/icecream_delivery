package com.cushmantracking.app.cushman.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by amitrai on 15/6/17.
 * User for :-
 */

public class SavedRouteResponse extends BaseBean {

    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<LatLang> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<LatLang> getData() {
        return data;
    }

    public void setData(List<LatLang> data) {
        this.data = data;
    }
}
