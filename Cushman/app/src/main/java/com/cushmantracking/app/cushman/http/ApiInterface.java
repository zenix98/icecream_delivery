package com.cushmantracking.app.cushman.http;

import com.cushmantracking.app.cushman.bean.LoginModal;
import com.cushmantracking.app.cushman.bean.RouteModal;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by amitRai on 17/04/2017.
 * all apis for this app
 */

public interface ApiInterface {

//    @Field("DeviceID") String DeviceID,
//    @Field("DeviceToken") String DeviceToken,
//    @Field("DeviceType") String DeviceType,
//    @Field("Email") String device_id,
//    @Field("Password") String Password

//    @FormUrlEncoded
    @POST("/api/Driver/AuthenticateDriver")
    Call<ResponseBody> driverLogin(@Body LoginModal modal);

    @POST("/api/Driver/GenerateRoute")
    Call<ResponseBody> crateRoute(@Body RouteModal routeModal);

    @FormUrlEncoded
    @POST("/api/Driver/GetSavedRoutes")
    Call<ResponseBody> getRoutes(@Field("AccessToken")String AccessToken);

    @FormUrlEncoded
    @POST("/api/Driver/GetSavedRouteCoordinates")
    Call<ResponseBody> getRouteDetail(@Field("AccessToken")String AccessToken,
                                      @Field("DriverRouteID")int DriverRouteID );





}
