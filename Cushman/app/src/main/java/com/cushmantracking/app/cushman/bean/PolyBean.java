package com.cushmantracking.app.cushman.bean;

import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.Polyline;

/**
 * Created by amitrai on 31/5/17.
 */

public class PolyBean {
    Marker marker;
    Polyline polyline;

    public PolyBean(Marker marker, Polyline polyline){
        this.marker = marker;
        this.polyline = polyline;
    }

    public Marker getMarker() {
        return marker;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }

    public Polyline getPolyline() {
        return polyline;
    }

    public void setPolyline(Polyline polyline) {
        this.polyline = polyline;
    }
}
