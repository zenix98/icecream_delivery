package com.cushmantracking.app.cushman.ui.driver.activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.runtimepermission.PermissionHandler;
import com.app.runtimepermission.PermissionListener;
import com.cushmantracking.app.cushman.AppInitials;
import com.cushmantracking.app.cushman.R;
import com.cushmantracking.app.cushman.bean.BaseBean;
import com.cushmantracking.app.cushman.db.AppPreference;
import com.cushmantracking.app.cushman.http.ApiInterface;
import com.cushmantracking.app.cushman.http.ConnectionManager;
import com.cushmantracking.app.cushman.http.request.NotificationRequest;
import com.cushmantracking.app.cushman.http.request.RouteRequest;
import com.cushmantracking.app.cushman.listeners.ApiCallback;
import com.cushmantracking.app.cushman.ui.driver.fragments.BaseFragment;
import com.cushmantracking.app.cushman.ui.driver.fragments.LoginFragment;
import com.cushmantracking.app.cushman.ui.driver.fragments.RouteListFragment;
import com.cushmantracking.app.cushman.util.AppConstants;
import com.cushmantracking.app.cushman.util.Utility;
import com.google.gson.Gson;

import javax.inject.Inject;

public class BaseActivity extends AppCompatActivity {



    @Inject
    ApiInterface service;

    @Inject
    Utility utility;

    @Inject
    Gson gson;

    @Inject
    ConnectionManager connectionManager;

    @Inject
    AppConstants constants;

    @Inject
    AppPreference preference;


    @Inject
    NotificationRequest notificationRequest;

    @Inject
    RouteRequest routeRequest;


    public TextView txt_header = null;
    public ImageView img_header = null;

    public CoordinatorLayout layout_snackbar = null;

    private final String TAG = getClass().getSimpleName();
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_home);
        AppInitials.getContext().getInjector().inject(this);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);




//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.setDrawerListener(toggle);
//        toggle.syncState();
//
//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);

        init();

    }

    /**
     * initializing view elements.
     */
    private void init(){
        layout_snackbar = (CoordinatorLayout) findViewById(R.id.layout_snackbar);

        PermissionHandler.getAllPermissions(this, new PermissionListener() {
            @Override
            public void onGranted(String message) {

            }

            @Override
            public void onError(String errorMessage) {

            }

            @Override
            public void onRejected(String message) {

            }
        });



        if (preference.getToken().isEmpty())
            replaceFragment(new LoginFragment(), true);
        else
            replaceFragment(new RouteListFragment(), true);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

//    @SuppressWarnings("StatementWithEmptyBody")
//    @Override
//    public boolean onNavigationItemSelected(MenuItem item) {
//        // Handle navigation view item clicks here.
//        int id = item.getItemId();
//
//        if (id == R.id.nav_camera) {
//            // Handle the camera action
//        } else if (id == R.id.nav_gallery) {
//
//        } else if (id == R.id.nav_slideshow) {
//
//        } else if (id == R.id.nav_manage) {
//
//        } else if (id == R.id.nav_routes) {
//            getRoutes();
//        } else if (id == R.id.nav_logout) {
//            logout();
//        }
//
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        drawer.closeDrawer(GravityCompat.START);
//        return true;
//    }

    private void getRoutes() {
        routeRequest.getAllRoutes(preference.getToken(), new ApiCallback() {
            @Override
            public void onRequestSuccess(BaseBean bean) {
                Log.e(TAG, ""+bean);
            }

            @Override
            public void onRequestFailed(String message) {
                Log.e(TAG, ""+message);
            }
        });
    }


    /**
     * replaces the previous fragment with the new one
     * @param fragment to be replace.
     * @param saveInBackStack should store in backstack or not
     */
    public void replaceFragment (BaseFragment fragment, boolean saveInBackStack){
        String backStateName =  fragment.getClass().getName();
        String tag = null;

        if(getSupportFragmentManager().getBackStackEntryCount() > 0){
            FragmentManager.BackStackEntry backEntry = getSupportFragmentManager().
                    getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount()-1);
            tag = backEntry.getName();

        }

        if(tag != null && tag.equalsIgnoreCase(backStateName)){
            Log.i(TAG, "already opened");
        }else {
            FragmentManager manager = getSupportFragmentManager();
            boolean fragmentPopped = manager.popBackStackImmediate (backStateName, 0);

            if (!fragmentPopped){ //fragment not in back stack, create it.
                FragmentTransaction ft = manager.beginTransaction();

                ft.setCustomAnimations(R.anim.slide_right_enter,
                        R.anim.slide_left_exit,
                        R.anim.slide_left_enter,
                        R.anim.slide_right_exit);

                ft.replace(R.id.container, fragment, backStateName);

                if (saveInBackStack){
                    ft.addToBackStack(backStateName);

                }

                ft.commit();
            }
        }
    }


    @Override
    public void onBackPressed() {

//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
                if (doubleBackToExitPressedOnce) {
                    finish();
                    return;
                }

                this.doubleBackToExitPressedOnce = true;
//                Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
                utility.showSnackBar(layout_snackbar, getString(R.string.close_app_msg));

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce=false;
                    }
                }, 2000);
            }else {
                super.onBackPressed();
            }
//        }

    }


    /**
     * logs out user from app
     * and closes all opened fragment.
     */
    public void logout(){
        preference.saveToken("");
        FragmentManager fm = getSupportFragmentManager();
        for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }

        replaceFragment(new LoginFragment(), false);
    }
}
