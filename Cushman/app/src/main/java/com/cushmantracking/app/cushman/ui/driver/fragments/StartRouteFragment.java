package com.cushmantracking.app.cushman.ui.driver.fragments;


import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cushmantracking.app.cushman.R;
import com.cushmantracking.app.cushman.bean.BaseBean;
import com.cushmantracking.app.cushman.bean.LatLang;
import com.cushmantracking.app.cushman.bean.SavedRouteResponse;
import com.cushmantracking.app.cushman.listeners.ApiCallback;
import com.cushmantracking.app.cushman.util.AppConstants;
import com.cushmantracking.app.cushman.util.DownloadWaypoints;
import com.cushmantracking.app.cushman.util.Logger;
import com.cushmantracking.app.cushman.util.Utility;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class StartRouteFragment extends BaseFragment implements OnMapReadyCallback,
        GoogleMap.OnMapClickListener, GoogleMap.OnMarkerClickListener, View.OnClickListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String PARM_ROUTE_ID = "route_id";

    private List<LatLang> list_waypoints = new ArrayList<>();

    private String TAG = getClass().getSimpleName();

    MapView mMapView;
    GoogleMap map;


    public static int route_id = -1;
    private View v;

    private Marker marker_me = null;


    private SavedRouteResponse current_route = null;

    private GoogleApiClient googleApiClient;


    public StartRouteFragment() {
        // Required empty public constructor
        AppConstants.FCM_TOKEN = FirebaseInstanceId.getInstance().getToken();
        Logger.e(TAG, "" + AppConstants.FCM_TOKEN);
    }


    @Override
    public void initView(View view) {
        TextView txt_start_stop = (TextView) view.findViewById(R.id.txt_start_stop);
        txt_start_stop.setOnClickListener(this);

    }

//    /**
//     * Use this factory method to create a new instance of
//     * this fragment using the provided parameters.
//     *
//     * @return A new instance of fragment CreateRouteFragment.
//     */
//    public static CreateRouteFragment newInstance(int route_id) {
//        CreateRouteFragment fragment = new CreateRouteFragment();
//        Bundle args = new Bundle();
//        args.putInt(PARM_ROUTE_ID, route_id);
//        fragment.setArguments(args);
//        return fragment;
//    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            route_id = getArguments().getInt(PARM_ROUTE_ID);
//            if (route_id != -1)
//            getRouteDetail(route_id);
        }
    }


    /**
     * fetches route detail from server
     * @param route_id id of the route to be fetched
     */
    private void getRouteDetail(int route_id) {
        String token = preference.getToken();
        if (!token.trim().isEmpty())
            routeRequest.getRouteDetail(token, route_id, new ApiCallback() {
                @Override
                public void onRequestSuccess(BaseBean bean) {
                    Logger.e(TAG, "" + bean);
                    current_route = (SavedRouteResponse) bean;

                    drawRoute(current_route);
                }

                @Override
                public void onRequestFailed(String message) {
                    utility.showSnackBar(activity.layout_snackbar, message);
                }
            });
        else
            utility.showSnackBar(activity.layout_snackbar, getString(R.string.error_not_logged_in));
    }


    private void drawRoute(SavedRouteResponse savedRouteResponse) {
        list_waypoints.addAll(savedRouteResponse.getData());

        try {
            if (Utility.isInternetAvailable(getContext())) {
                if (map != null)
                    map.getUiSettings().setZoomControlsEnabled(true);

                LatLng destination = new LatLng(
                        list_waypoints.get(list_waypoints.size() - 1).getLatitude(),
                        list_waypoints.get(list_waypoints.size() - 1).getLongitude());
                LatLng origin = new LatLng(
                        list_waypoints.get(0).getLatitude(),
                        list_waypoints.get(0).getLongitude());
                List<LatLang> list_stop_points = new ArrayList<>();

                for (int i = 1; i < list_waypoints.size() - 1; i++) {
                    list_stop_points.add(list_waypoints.get(i));
                }


                String url = utility.getDirectionsUrl(origin, destination, list_stop_points);

                DownloadWaypoints downloadTask = new DownloadWaypoints(map, getActivity());

                // Start downloading json data from Google Directions API
                downloadTask.execute(url);
            } else
                utility.showSnackBar(activity.layout_snackbar, "No Internet connection.");
        } catch (Exception exp) {
            exp.printStackTrace();
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (v == null) {
            v = inflater.inflate(R.layout.fragment_start_route, container, false);
            initView(v);
            mMapView = (MapView) v.findViewById(R.id.mapview);
            mMapView.onCreate(savedInstanceState);
            mMapView.getMapAsync(this); //this is important
        }


        googleApiClient = new GoogleApiClient.Builder(getContext())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();


        return v;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        if (route_id != -1)
            getRouteDetail(route_id);
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onMapClick(LatLng latLng) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
//        locationRequest.setInterval(1000);
        float DISPLACEMENT_FOR_UPDATE = 1;
        locationRequest.setSmallestDisplacement(DISPLACEMENT_FOR_UPDATE);


        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null){
            Drawable drawable = ContextCompat.getDrawable(getContext(), R.mipmap.nav_arrow);
            LatLang my_position = new LatLang(location.getLatitude(), location.getLongitude());
            if (marker_me == null){
                marker_me = map.addMarker(new MarkerOptions().
                        icon(BitmapDescriptorFactory.fromBitmap(((BitmapDrawable)drawable).getBitmap())).
                        position(new LatLng(location.getLatitude(), location.getLongitude())));

                map.moveCamera(CameraUpdateFactory
                        .newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 14));
            }
            else{
                utility.animateMarker(map, marker_me, new LatLng(location.getLatitude(), location.getLongitude()), false);
                double angle = utility.calculateAngle(my_position, list_waypoints.get(0));
                float angle_value = (float) angle;
                utility.rotateMarker(marker_me, angle_value, 120.0f);
            }

        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.txt_start_stop:

                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        googleApiClient.connect();
    }


    @Override
    public void onStop() {
        super.onStop();
        googleApiClient.disconnect();
    }


    private void updateLocationToServer(){

    }


//    public void animateMarker(final LatLng toPosition,final boolean hideMarke) {
//        final Handler handler = new Handler();
//        final long start = SystemClock.uptimeMillis();
//        Projection proj = map.getProjection();
//        Point startPoint = proj.toScreenLocation(marker_me.getPosition());
//        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
//        final long duration = 5000;
//
//        final Interpolator interpolator = new LinearInterpolator();
//
//        handler.post(new Runnable() {
//            @Override
//            public void run() {
//                long elapsed = SystemClock.uptimeMillis() - start;
//                float t = interpolator.getInterpolation((float) elapsed
//                        / duration);
//                double lng = t * toPosition.longitude + (1 - t)
//                        * startLatLng.longitude;
//                double lat = t * toPosition.latitude + (1 - t)
//                        * startLatLng.latitude;
//                marker_me.setPosition(new LatLng(lat, lng));
//
//                if (t < 1.0) {
//                    // Post again 16ms later.
//                    handler.postDelayed(this, 16);
//                } else {
//                    if (hideMarke) {
//                        marker_me.setVisible(false);
//                    } else {
//                        marker_me.setVisible(true);
//                    }
//                }
//            }
//        });
//    }
//

}
