package com.cushmantracking.app.cushman;

import android.app.Application;

import com.cushmantracking.app.cushman.util.AppConstants;
import com.cushmantracking.app.cushman.dagger.DaggerInjector;
import com.cushmantracking.app.cushman.dagger.Injector;
import com.cushmantracking.app.cushman.dagger.Modules;


/**
 * Created by amitrai on 17/4/17.
 * initial tasks for the app
 */

public class AppInitials extends Application {
    Injector injector;

    private static AppInitials context;

    @Override
    public void onCreate() {
        super.onCreate();

        context = this;

        injector = DaggerInjector.builder()
                .modules(new Modules(AppConstants.BASE_URL, getContext()))
                .build();

    }

    public Injector getInjector() {
        return injector;
    }


    public static AppInitials getContext(){
        return  context;
    }

}
