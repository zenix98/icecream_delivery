package com.cushmantracking.app.cushman.dagger;

import android.content.Context;

import com.cushmantracking.app.cushman.db.AppPreference;
import com.cushmantracking.app.cushman.http.ApiInterface;
import com.cushmantracking.app.cushman.http.ConnectionManager;
import com.cushmantracking.app.cushman.http.request.LoginRequest;
import com.cushmantracking.app.cushman.http.request.NotificationRequest;
import com.cushmantracking.app.cushman.http.request.RouteRequest;
import com.cushmantracking.app.cushman.util.AppConstants;
import com.cushmantracking.app.cushman.util.Utility;
import com.google.gson.Gson;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by amitRai on 17/04/2017.
 * dependencies for the app.
 */

@Module
public class Modules {

    private String mBaseUrl;
    private Context context;

    // Constructor needs one parameter to instantiate.
    public Modules(String baseUrl, Context context) {
        this.mBaseUrl = baseUrl;
        this.context = context;
    }

    @Provides
    @Singleton
    NotificationRequest provideNotificationRequest(){
        return new NotificationRequest();
    }

    @Provides
    @Singleton
    LoginRequest provideLoginRequest(){
        return new LoginRequest();
    }


    @Provides
    @Singleton
    RouteRequest provideRouteRequest(){
        return new RouteRequest();
    }


    @Provides
    @Singleton
    Utility provideUtility(){
        return new Utility();
    }

    @Provides
    @Singleton
    AppConstants provideAppConstants(){
        return new AppConstants();
    }


    @Provides
    @Singleton
    Retrofit provideRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(mBaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    ApiInterface provideIterface(){
        return provideRetrofit().create(ApiInterface.class);
    }

    @Provides
    @Singleton
    ConnectionManager provideConnection(){
        return new ConnectionManager();
    }

    @Provides
    @Singleton
    String provideDisplaceMsg (){
        return "message from provider";
    }

    @Provides
    @Singleton
    AppPreference providePreference(){
        return new AppPreference(context);
    }

    @Provides
    @Singleton
    Gson provideGson(){
        return new Gson();
    }

}