package com.cushmantracking.app.cushman.fcm;

import android.support.annotation.NonNull;
import android.util.Log;

import com.cushmantracking.app.cushman.util.AppConstants;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by amitrai on 1/5/17.
 * this class is used for :- register app with fcm
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private final String TAG = getClass().getSimpleName();

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshed_token = FirebaseInstanceId.getInstance().getToken();
        if (refreshed_token != null)
            sendTokenToServer(refreshed_token);
    }

    /**
     * sends fcm token to server
     * @param token to be sent on server
     */
    private void sendTokenToServer(@NonNull String token){
        Log.e(TAG, "fcm token "+token);
        AppConstants.FCM_TOKEN = token;
    }
}
