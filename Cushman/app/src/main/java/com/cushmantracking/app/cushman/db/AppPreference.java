package com.cushmantracking.app.cushman.db;

import android.content.Context;
import android.content.SharedPreferences;

import com.cushmantracking.app.cushman.bean.LoginModal;

/**
 * Created by amitrai on 24/5/17.
 * this class is used for :-
 */

public class AppPreference {


    private SharedPreferences mPref;
    private SharedPreferences.Editor mEditor;

    private final String ACCESS_TOKEN = "driver_token";
    private final String DRIVER_NAME = "driver_name";


    /**
     * cunstructor of the class to initiate basic tasks
     * @param context of the application.
     */
    public AppPreference(Context context) {
        int mPRIVATE_MODE = 0;
        String mPREF_NAME = "CUSHMAN";
        mPref = context.getSharedPreferences(mPREF_NAME, mPRIVATE_MODE);
        mEditor = mPref.edit();
        mEditor.apply();
    }

    /**
     * Stores driver info into preferences.
     * @param modal to store driver data
     */
    public void saveDriverInfo(LoginModal modal){
        mEditor.putString(ACCESS_TOKEN, modal.getAccessToken());
        mEditor.putString(DRIVER_NAME, modal.getName());
        mEditor.commit();
    }

    /**
     * Stores driver info into preferences.
     * @param token to be saved
     */
    public void saveToken(String token){
        mEditor.putString(ACCESS_TOKEN, token);
        mEditor.commit();
    }

    /**
     * returns access token if driver is logged in
     * @return  token and empty string in case of not logged in.
     */
    public String getToken(){
        return mPref.getString(ACCESS_TOKEN, "");
    }

    /**
     * returns name of the driver
     * @return  name of the driver.
     */
    public String getDriverName(){
        return mPref.getString(DRIVER_NAME, "");
    }
}
