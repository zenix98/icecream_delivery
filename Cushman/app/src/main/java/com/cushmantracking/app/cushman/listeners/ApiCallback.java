package com.cushmantracking.app.cushman.listeners;


import com.cushmantracking.app.cushman.bean.BaseBean;

/**
 * Created by amit rai on 20/3/17.
 * callbacks for api
 */

public interface ApiCallback {
    void onRequestSuccess(BaseBean bean);
    void onRequestFailed(String message);
}
