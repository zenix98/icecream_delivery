package com.cushmantracking.app.cushman.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by amitrai on 14/6/17.
 * User for :-
 */

public class RouteResponseBean extends BaseBean{
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<DriverRouteBean> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DriverRouteBean> getData() {
        return data;
    }

    public void setData(List<DriverRouteBean> data) {
        this.data = data;
    }
}
