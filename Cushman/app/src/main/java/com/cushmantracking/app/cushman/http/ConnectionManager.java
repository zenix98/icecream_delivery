package com.cushmantracking.app.cushman.http;


import android.util.Log;

import com.cushmantracking.app.cushman.listeners.BaseListener;

import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by amitRai on 17/04/2017.
 * manages connections from web to app
 */

public class ConnectionManager  {
    private static final String STR_NO_CONNECTION ="Trouble reaching  server,No internet connection.";
    private static ConnectionManager mConnectionManger;
    private Call<ResponseBody> enqueueCall;

    private final String TAG = getClass().getSimpleName();

    public static ConnectionManager getConnectionInstance(Call<ResponseBody> call) {
        if (mConnectionManger == null) {
            mConnectionManger = new ConnectionManager();
            mConnectionManger.setEnqueueCall(call);
            return mConnectionManger;
        } else{
            mConnectionManger.setEnqueueCall(call);
            return mConnectionManger;
        }
    }

    private void setEnqueueCall(Call<ResponseBody> mCall) {

        this.enqueueCall = mCall;
    }


    public void callApi(final BaseListener.OnWebServiceCompleteListener mListener){
        this.enqueueCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if(response.isSuccessful()){
                    mListener.onWebServiceComplete(response.body());
                }else{
                    try {
                        String error = response.errorBody().string();
                        JSONObject object = new JSONObject(error);

                        String message = "";
                        if (object.has("Message"))
                            message = object.getString("Message");
                        Log.e(TAG ,"error message "+message);
                        mListener.onWebStatusFalse(message);

                    }catch (Exception exp){
                        exp.printStackTrace();
                        mListener.onWebStatusFalse("Ops! An Error Occurred");
                    }
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(t instanceof IOException){
                    mListener.onWebStatusFalse(ConnectionManager.STR_NO_CONNECTION);
                    return;
                }
                mListener.onWebStatusFalse(t.getMessage());
            }
        });
    }
}
