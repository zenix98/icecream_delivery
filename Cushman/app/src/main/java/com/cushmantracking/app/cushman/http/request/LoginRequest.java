package com.cushmantracking.app.cushman.http.request;

import android.util.Log;

import com.cushmantracking.app.cushman.bean.LoginModal;
import com.cushmantracking.app.cushman.http.ConnectionManager;
import com.cushmantracking.app.cushman.listeners.ApiCallback;
import com.cushmantracking.app.cushman.listeners.BaseListener;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by amitrai on 12/6/17.
 * api request for login and related calls
 */

public class LoginRequest extends BaseRequest {

    private final String TAG = getClass().getSimpleName();

    /**
     * attempts login on server.
     * @param modal username of the driver
     * @param callback app callback listener
     */
    public void attemptLoginDriver(LoginModal modal, final ApiCallback callback){
        Call<ResponseBody> callNotication=getAPIClient().driverLogin(
                modal);
        ConnectionManager connectionManager= ConnectionManager.getConnectionInstance(callNotication);
        connectionManager.callApi(new BaseListener.OnWebServiceCompleteListener() {
            @Override
            public void onWebServiceComplete(ResponseBody baseObject) {
                Log.e(TAG, ""+baseObject);
                try{
                    JSONObject jsonObject = new JSONObject(baseObject.string());

                    if (jsonObject.has("data")){
                        LoginModal loginModal =getGsonBuilder().fromJson(jsonObject.getString("data"), LoginModal.class);
                        callback.onRequestSuccess(loginModal);

                    }else
                        callback.onRequestFailed("Error occurred while parsing");

                }catch (Exception exp){
                    exp.printStackTrace();
                    callback.onRequestFailed("Error occurred while parsing");
                }

            }
            @Override
            public void onWebStatusFalse(String message) {
                callback.onRequestFailed(message);
            }
        });
    }
}
