package com.cushmantracking.app.cushman.fcm;

import com.cushmantracking.app.cushman.util.Logger;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by amitrai on 3/5/17.
 * this class is used for :- receiving messages from fcm
 */

public class FcmMessageService extends FirebaseMessagingService {

    private final String TAG = getClass().getSimpleName();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Logger.e(TAG, "From: " + remoteMessage.getFrom());
//        Logger.e(TAG, "From: " + remoteMessage.getData());
    }

//
//    /**
//     * creates notification for the app
//     * @param message notification message
//     */
//    private void createNotification(String message){
//
//        NotificationManager mNotificationManager = (NotificationManager)
//                this.getSystemService(Context.NOTIFICATION_SERVICE);
//
//        Intent i = new Intent(getApplicationContext(), HomeActivity.class);
//        i.putExtra(HomeActivity.KEY_NOTIFICATICATION, true);
//
//
//        PendingIntent contentIntent = PendingIntent.getActivity(this, 1, i, PendingIntent.FLAG_CANCEL_CURRENT);
//
//        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext());
//        mBuilder.setContentTitle(message)
//                .setContentText(message)
//                .setSmallIcon(R.mipmap.notification)
//                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher_round))
//                .setPriority(Notification.PRIORITY_HIGH)
//                .build();
//
//        mBuilder.setContentIntent(contentIntent);
//        mNotificationManager.notify(0, mBuilder.build());
//    }

}
