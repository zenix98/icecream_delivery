package com.cushmantracking.app.cushman.http.request;

import android.util.Log;

import com.cushmantracking.app.cushman.bean.BaseBean;
import com.cushmantracking.app.cushman.bean.RouteModal;
import com.cushmantracking.app.cushman.bean.RouteResponseBean;
import com.cushmantracking.app.cushman.bean.SavedRouteResponse;
import com.cushmantracking.app.cushman.http.ConnectionManager;
import com.cushmantracking.app.cushman.listeners.ApiCallback;
import com.cushmantracking.app.cushman.listeners.BaseListener;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by amitrai on 13/6/17.
 * will be used for :- request related api hits
 */

public class RouteRequest extends BaseRequest {

    private final String TAG = getClass().getSimpleName();

    /**
     * attempts login on server.
     * @param modal username of the driver
     * @param callback app callback listener
     */
    public void saveRoute(RouteModal modal, final ApiCallback callback){
        Call<ResponseBody> callSaveRoute=getAPIClient().crateRoute(
                modal);
        ConnectionManager connectionManager= ConnectionManager.getConnectionInstance(callSaveRoute);
        connectionManager.callApi(new BaseListener.OnWebServiceCompleteListener() {
            @Override
            public void onWebServiceComplete(ResponseBody baseObject) {
                Log.e(TAG, ""+baseObject);
                try{
                    JSONObject jsonObject = new JSONObject(baseObject.string());

                    if (jsonObject.has("Message")){
                        BaseBean bean = new BaseBean();
                        bean.setMessage(jsonObject.getString("Message"));
                        callback.onRequestSuccess(bean);
                    }else
                        callback.onRequestFailed("Error occurred while parsing");

                }catch (Exception exp){
                    exp.printStackTrace();
                    callback.onRequestFailed("Error occurred while parsing");
                }

            }
            @Override
            public void onWebStatusFalse(String message) {
                callback.onRequestFailed(message);
            }
        });
    }

    /**
     * gets all the routes from the server.
     * @param access_token access token
     * @param callback to convey response
     */
    public void getAllRoutes(String access_token, final ApiCallback callback){
        Call<ResponseBody> callGetRoute=getAPIClient().getRoutes(access_token);
        ConnectionManager connectionManager= ConnectionManager.getConnectionInstance(callGetRoute);
        connectionManager.callApi(new BaseListener.OnWebServiceCompleteListener() {
            @Override
            public void onWebServiceComplete(ResponseBody baseObject) {
                try {

                    JSONObject jsonObject = new JSONObject(baseObject.string());
                    Log.e(TAG, "success "+jsonObject);

                    if (jsonObject.has("data")){
                        RouteResponseBean routeModal =getGsonBuilder().fromJson(jsonObject.toString(), RouteResponseBean.class);
                        callback.onRequestSuccess(routeModal);

                    }else
                        callback.onRequestFailed("Error occurred while parsing");

                }catch (Exception exp){
                    exp.printStackTrace();
                    callback.onRequestFailed("Error occurred while parsing");
                }

            }

            @Override
            public void onWebStatusFalse(String message) {
                Log.e(TAG, "error "+message);
                callback.onRequestFailed(message);
            }
        });
    }


    public void getRouteDetail(String AccessToken, int DriverRouteID, final ApiCallback callback){
        Call<ResponseBody> callGetRoute=getAPIClient().getRouteDetail(AccessToken, DriverRouteID);
        ConnectionManager connectionManager= ConnectionManager.getConnectionInstance(callGetRoute);
        connectionManager.callApi(new BaseListener.OnWebServiceCompleteListener() {
            @Override
            public void onWebServiceComplete(ResponseBody baseObject) {
                try {

                    JSONObject jsonObject = new JSONObject(baseObject.string());
                    Log.e(TAG, "success "+jsonObject);

                    if (jsonObject.has("data")){
                        SavedRouteResponse routeModal =getGsonBuilder().fromJson(jsonObject.toString(), SavedRouteResponse.class);
                        callback.onRequestSuccess(routeModal);

                    }else
                        callback.onRequestFailed("Error occurred while parsing");

                }catch (Exception exp){
                    exp.printStackTrace();
                    callback.onRequestFailed("Error occurred while parsing");
                }

            }

            @Override
            public void onWebStatusFalse(String message) {
                Log.e(TAG, "error "+message);
                callback.onRequestFailed(message);
            }
        });

    }


}
